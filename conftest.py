import pytest

from run import create_app
from db import init_db

@pytest.fixture
def app():
    app = create_app()
    return app

@pytest.fixture(scope='module', autouse=True)
def load_db():
    init_db()