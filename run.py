import logging
from flask import Flask
from flask_migrate import Migrate
from flask_cors import CORS
from app.utils.errors import register_custom_error_handler
from db import Session
from app import models
from app.v1.app import create_api as create_v1

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def create_app():
    app = Flask(__name__)
    CORS(app)
    register_custom_error_handler(app)
    app = create_v1(app)
    return app
    
if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)