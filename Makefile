include .env
export $(shell sed 's/=.*//' .env)
export NS=$(REALM)-$(REGISTRY_TYPE)-$(REGISTRY_AREA)-$(REGISTRY_MODULE)-$(REGISTRY_APP)
export NAMESPACE=`echo "$(NS)" | tr A-Z a-z`

# TEXT/COLORS
GREEN:=$(shell tput setaf 2)
BOLD:=$(shell tput bold)
RESET:=$(shell tput sgr0)

# MESSAGES
SUCCESS_MESSAGE:=OK
SUCCESS:=$(GREEN)$(SUCCESS_MESSAGE)$(RESET)

# PYTHON
PYTHON_VERSION=$(shell python3 --version)
ifndef PYTHON_VERSION
$(error Python 3 não instalado (https://www.python.org/downloads/))
endif

# .PHONY: clean help

help:
	@echo "$(BOLD)install$(RESET): execute prepare work station for development"
	@echo "$(BOLD)clear$(RESET): clear temporaries files"
	@echo "$(BOLD)run$(RESET): execute API"
	@echo "$(BOLD)test$(RESET): execute tests from API"
	@echo ""
	@echo "$(BOLD)help$(RESET): show this message"

clear:
	@printf "Cleaning temporaries files... "
	@rm -f dist/*.gz
	@rm -rfd *.egg-info
	@find . -type f -name '*.pyc' -delete
	@find . -type f -name '*.log' -delete
	@echo "$(SUCCESS)"

env-create:
	@printf "Creating virtual place venv... "
	@rm -rfd venv
	@virtualenv -q -p python3.6 venv
	@echo "$(SUCCESS)"
	
system-packages:
	@printf "Installing 'pip' and 'virtualenv'... "
	@curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	@python3  -q  get-pip.py 1> /dev/null
	@pip install -q -U pip
	@pip install -q virtualenv 
	@rm get-pip.py
	@echo "$(SUCCESS)"

packages: env-create
	@printf "Installing libs... "
	@venv/bin/pip install -q --no-cache-dir -r requirements.txt
	@echo "$(SUCCESS)"
	
install: clear system-packages packages
	@echo "============================================"
	@echo "Already for development"
	@echo ""
	@echo "Tape for activation: "
	@echo ""
	@echo "source venv/bin/activate"
	@echo "============================================"
	
test:
	@cp .env.test .env
	@venv/bin/python -m pytest

coverage: test
	@venv/bin/coverage erase
	@venv/bin/python -m pytest --cov ./app
	@venv/bin/coverage xml -i

run: 
	@cp .env.prod .env
	@venv/bin/python run.py
