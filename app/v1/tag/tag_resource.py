from flask_restplus import Resource, Namespace
from flask import request
from app.v1.tag.tag_service import TagService

service = TagService()
route_name = 'tags'
ns = Namespace(name=route_name)

@ns.route('')
class TagListResource(Resource):

    @ns.doc(f'read a list of {route_name}')
    def get(self):
        return service.page(request.args)

    @ns.doc(f'create {route_name}')
    def post(self):
        return service.create(payload=request.json)
      
@ns.route('/<int:id>')
@ns.doc(params={'id': f'id of {route_name}'})
class TagResource(Resource):

    @ns.doc(f'read {route_name} by id')
    def get(self, id):
        return service.get(id)

    @ns.doc(f'update {route_name}')
    def put(self, id):
        return service.update(id=id, payload=request.json)
        
    @ns.doc(f'delete {route_name}')
    def delete(self, id):
        return service.delete(id=id)