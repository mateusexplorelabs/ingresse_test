from app.utils.services import Service
from app.v1.tag.tag_model import Tag, TagSchema

class TagService(Service):

    __model__ = Tag
    __schema__ = TagSchema()
