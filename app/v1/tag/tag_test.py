import pytest
import json

from db import Session
from app.v1.tag.tag_model import Tag

route = "/v1/tags"

@pytest.fixture(scope='module', autouse=True)
def load_data():
    Session.add(Tag(id=1, name="Tag 1"))
    Session.add(Tag(id=2, name="Tag 2"))
    Session.add(Tag(id=3, name="Tag 3"))
    Session.add(Tag(id=4, name="Tag 4"))
    Session.commit()
    yield
    Session.remove()

def test_get_without_args(client):
    response = client.get(route)
    assert response.status_code == 200
    assert len(response.json['data']) == 4
    assert response.json['data'][0]['name'] == "Tag 1"

def test_get_with_offset(client):
    response = client.get(route+'?offset=1')
    assert response.status_code == 200
    assert len(response.json['data']) == 3
    assert response.json['data'][0]['name'] == "Tag 2"

def test_get_with_limit(client):
    response = client.get(route+'?limit=20')
    assert response.status_code == 200
    assert len(response.json['data']) == 4
    assert response.json['data'][0]['name'] == "Tag 1"

def test_get_with_all_args(client):
    response = client.get(route+'?limit=2&offset=1')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Tag 2"

def test_get_by_existent_id(client):
    response = client.get(route+'/1')
    assert response.status_code == 200
    assert len(response.json) == 1
    assert response.json['data']['name'] == "Tag 1"

def test_get_by_unexistent_id(client):
    response = client.get(route+'/999')
    assert response.status_code == 404
    assert response.json['message'] == "Resource not found"

def test_create_tag_valid_payload(client):
    response = client.post(route, data=json.dumps(dict(
        name='abc'
    )), content_type='application/json')
    assert response.status_code == 200

def test_create_tag_invalid_payload(client):
    response = client.post(route, data=json.dumps(dict(
        undef='undefined'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Request has no valid schema: <class 'app.v1.tag.tag_model.Tag'>"

def test_create_tag_without_payload(client):
    response = client.post(route)
    assert response.status_code == 400
    assert response.json['message'] == "Empty request"

def test_update_tag_valid_payload(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        id=1,
        name='def'
    )), content_type='application/json')
    assert response.status_code == 200

def test_update_tag_valid_payload_diff_id(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        id=2,
        name='ghi'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Both ids must be the same. id param: 1 != id request: 2"

def test_update_tag_invalid_payload(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        undef='undefined'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Request has no valid schema: <class 'app.v1.tag.tag_model.Tag'>"

def test_update_tag_without_payload(client):
    response = client.put(route+'/1')
    assert response.status_code == 400
    assert response.json['message'] == "Empty request"

def test_update_tag_that_not_exists(client):
    response = client.put(route+'/999', data=json.dumps(dict(
        id=999,
        name='jkl'
    )), content_type='application/json')
    assert response.status_code == 200

def test_delete_by_existent_id(client):
    response = client.delete(route+'/1')
    assert response.status_code == 200

def test_delete_by_unexistent_id(client):
    response = client.delete(route+'/888')
    assert response.status_code == 404
    assert response.json['message'] == "Resource not found"