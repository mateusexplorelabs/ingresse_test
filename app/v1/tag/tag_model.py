from db import Base
from sqlalchemy import Column, Integer, String
from marshmallow_sqlalchemy import ModelSchema

class Tag(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250), nullable=False)

class TagSchema(ModelSchema):
    class Meta:
        model = Tag