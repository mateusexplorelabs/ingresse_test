import pytest
import json
import datetime

from db import Session
from app.v1.event.event_model import Event, EventTag
from app.v1.session.session_model import Session as session_model
from app.v1.tag.tag_model import Tag

route = "/v1/events"

@pytest.fixture(scope='module', autouse=True)
def load_data():
    Session.add(Tag(id=1, name="tag_test"))

    Session.add(Event(id=1, name="Test 1", place="Adress 1"))
    Session.add(Event(id=2, name="Test 2", place="Adress 2", interested=10))
    Session.add(Event(id=3, name="Test 3", place="Adress 3", interested=10))
    Session.add(Event(id=4, name="Test 4", place="place_test"))
    Session.add(Event(id=5, name="Test 5", place="Adress 5"))
    Session.add(Event(id=6, name="Test 6", place="Adress 6"))
    Session.add(Event(id=7, name="Test 7", place="Adress 7"))
    Session.add(Event(id=8, name="Test 8", place="Adress 8"))
    Session.add(Event(id=9, name="Test 9", place="Adress 9"))
    Session.add(Event(id=10, name="Test 10", place="Adress 10"))
    Session.add(Event(id=11, name="Test 11", place="Adress 11"))
    Session.add(Event(id=12, name="name_test", place="name_test"))

    Session.add(EventTag(id=1, event_id=5, tag_id=1))
    Session.add(EventTag(id=2, event_id=6, tag_id=1))

    Session.add(session_model(id=1, event_id=8, date_time=datetime.datetime.strptime('2020-01-20', '%Y-%m-%d')))
    Session.add(session_model(id=2, event_id=8, date_time=datetime.datetime.strptime('2020-01-15', '%Y-%m-%d')))
    Session.add(session_model(id=3, event_id=9, date_time=datetime.datetime.strptime('2020-01-10', '%Y-%m-%d')))

    Session.commit()
    yield
    Session.remove()

def test_get_without_args(client):
    response = client.get(route)
    assert response.status_code == 200
    assert len(response.json['data']) == 10
    assert response.json['data'][0]['name'] == "Test 1"

def test_get_with_offset(client):
    response = client.get(route+'?offset=1')
    assert response.status_code == 200
    assert len(response.json['data']) == 10
    assert response.json['data'][0]['name'] == "Test 2"

def test_get_with_limit(client):
    response = client.get(route+'?limit=20')
    assert response.status_code == 200
    assert len(response.json['data']) == 12
    assert response.json['data'][0]['name'] == "Test 1"

def test_get_with_all_base_args(client):
    response = client.get(route+'?limit=5&offset=1')
    assert response.status_code == 200
    assert len(response.json['data']) == 5
    assert response.json['data'][0]['name'] == "Test 2"

def test_get_with_name(client):
    response = client.get(route+'?name=name_test')
    assert response.status_code == 200
    assert len(response.json['data']) == 1
    assert response.json['data'][0]['name'] == "name_test"

def test_get_with_place(client):
    response = client.get(route+'?place=place_test')
    assert response.status_code == 200
    assert len(response.json['data']) == 1
    assert response.json['data'][0]['name'] == "Test 4"

def test_get_with_interested(client):
    response = client.get(route+'?interested=1')
    assert response.status_code == 200
    assert len(response.json['data']) == 0

def test_get_with_interested_more_then(client):
    response = client.get(route+'?interested_more_then=2')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Test 2"

def test_get_with_tag_name(client):
    response = client.get(route+'?tag_name=tag_test')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Test 5"

def test_get_with_start_date(client):
    response = client.get(route+'?start_date=2020-01-01')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Test 8"

def test_get_with_end_date(client):
    response = client.get(route+'?end_date=2020-01-30')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Test 8"

def test_get_with_start_and_end_date(client):
    response = client.get(route+'?start_date=2020-01-01&end_date=2020-01-30')
    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['data'][0]['name'] == "Test 8"

def test_get_by_existent_id(client):
    response = client.get(route+'/1')
    assert response.status_code == 200

def test_get_by_unexistent_id(client):
    response = client.get(route+'/999')
    assert response.status_code == 404
    assert response.json['message'] == "Resource not found"

def test_create_event_valid_payload(client):
    response = client.post(route, data=json.dumps(dict(
        name='abc',
        place='adress acb'
    )), content_type='application/json')
    assert response.status_code == 200

def test_create_event_invalid_payload(client):
    response = client.post(route, data=json.dumps(dict(
        undef='undefined'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Request has no valid schema: <class 'app.v1.event.event_model.Event'>"

def test_create_event_without_payload(client):
    response = client.post(route)
    assert response.status_code == 400
    assert response.json['message'] == "Empty request"

def test_update_event_valid_payload(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        id=1,
        name='def',
        place='adress def'
    )), content_type='application/json')
    assert response.status_code == 200

def test_update_event_valid_payload_diff_id(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        id=2,
        name='ghi',
        place='adress ghi'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Both ids must be the same. id param: 1 != id request: 2"

def test_update_event_invalid_payload(client):
    response = client.put(route+'/1', data=json.dumps(dict(
        undef='undefined'
    )), content_type='application/json')
    assert response.status_code == 422
    assert response.json['message'] == "Request has no valid schema: <class 'app.v1.event.event_model.Event'>"

def test_update_event_without_payload(client):
    response = client.put(route+'/1')
    assert response.status_code == 400
    assert response.json['message'] == "Empty request"

def test_update_event_that_not_exists(client):
    response = client.put(route+'/999', data=json.dumps(dict(
        id=999,
        name='jkl',
        place='adress jkl'
    )), content_type='application/json')
    assert response.status_code == 200

def test_delete_by_existent_id(client):
    response = client.delete(route+'/1')
    assert response.status_code == 200

def test_delete_by_unexistent_id(client):
    response = client.delete(route+'/888')
    assert response.status_code == 404
    assert response.json['message'] == "Resource not found"

def test_interested(client):
    response = client.get(route+'/interested/12')
    assert response.status_code == 200
    assert response.json['data']['interested'] == 1