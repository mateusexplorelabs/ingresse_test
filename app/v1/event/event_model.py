from db import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from app.v1.session.session_model import Session, SessionSchema
from app.v1.tag.tag_model import Tag, TagSchema
from marshmallow_sqlalchemy import ModelSchema
from marshmallow_sqlalchemy.fields import Nested

class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(250), nullable=False)
    sessions = relationship("Session", uselist=True, cascade="all")
    place = Column(String(250), nullable=False)
    events_tags = relationship("EventTag", uselist=True)
    interested = Column(Integer)

class EventTag(Base):
    __tablename__ = 'events_tags'
    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, ForeignKey('events.id'))
    tag_id = Column(Integer, ForeignKey('tags.id'))
    tag = relationship("Tag")

class EventTagSchema(ModelSchema):
    tag = Nested(TagSchema)
    class Meta:
        model = EventTag

class EventSchema(ModelSchema):
    sessions = Nested(SessionSchema, many=True)
    events_tags = Nested(EventTagSchema, many=True)
    class Meta:
        model = Event
