from flask_restplus import Resource, Namespace
from flask import request
from app.v1.event.event_service import EventService

service = EventService()
route_name = 'events'
ns = Namespace(name=route_name)

@ns.route('')
class EventListResource(Resource):

    @ns.doc(f'read a list of {route_name}')
    def get(self):
        return service.get_by_where(request.args)

    @ns.doc(f'create {route_name}')
    def post(self):
        return service.create(payload=request.json)

@ns.route('/<int:id>')
@ns.doc(params={'id': f'id of {route_name}'})
class EventResource(Resource):

    @ns.doc(f'read {route_name} by id')
    def get(self, id):
        return service.get(id)

    @ns.doc(f'update {route_name}')
    def put(self, id):
        return service.update(id=id, payload=request.json)
        
    @ns.doc(f'delete {route_name}')
    def delete(self, id):
        return service.delete(id=id)

@ns.route('/interested/<int:id>')
@ns.doc(params={'id': f'id of {route_name}'})
class InterestedResource(Resource):

    @ns.doc(f'interested {route_name}')
    def get(self, id):
        return service.interested(id)