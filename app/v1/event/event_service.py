import logging
from app.utils.services import Service
from app.v1.event.event_model import Event, EventTag, EventSchema
from app.v1.tag.tag_model import Tag
from app.v1.session.session_model import Session as SessionModel
from db import Session

logger = logging.getLogger(__name__)

class EventService(Service):

    __model__ = Event
    __schema__ = EventSchema()

    def interested(self, id):
        logger.info(f'Interested of {self.__model__}')
        model = self.get(id=id, only_search=True)
        model.interested = model.interested + 1 if model.interested else 1
        Session.merge(model)
        Session.commit()
        return self._make_model_response(model)

    def get_by_where(self, args):
        logger.info(f'Get by where of {self.__model__}')
        query = Session.query(self.__model__)

        if args.get('name'):
            query = query.filter(Event.name.like(args.get('name')))

        if args.get('place'):
            query = query.filter(Event.place.like(args.get('place')))

        if args.get('interested'):
            query = query.filter(Event.interested == args.get('interested'))

        if args.get('interested_more_then'):
            query = query.filter(Event.interested >= args.get('interested_more_then'))

        if args.get('tag_name'):
            query = query.join(Event.events_tags). \
                join(EventTag.tag). \
                filter(Tag.name.like(args.get('tag_name')))

        if args.get('start_date') or args.get('end_date'):

            query = query.join(Event.sessions)

            if args.get('start_date'):
                query = query.filter(SessionModel.date_time >= args.get('start_date'))

            if args.get('end_date'):
                query = query.filter(SessionModel.date_time <= args.get('end_date'))

        return self.page(args, query)