from flask_restplus import Api

def create_api(app):

    api = Api(app,
            version='1.0',
            title='Events Management API',
            description='Events Management API is destinated for a interview test on Ingresse, is a little solution for events search, save, update and delete. This API also manage a interests views on witch event, who everyone can be interested.')

    from app.v1.ping.ping_resource import ns as ping_v1_resource, route_name as ping_path
    api.add_namespace(ping_v1_resource, path=f'/v1/{ping_path}')

    from app.v1.event.event_resource import ns as event_v1_resource, route_name as event_path
    api.add_namespace(event_v1_resource, path=f'/v1/{event_path}')

    from app.v1.tag.tag_resource import ns as tag_v1_resource, route_name as tag_path
    api.add_namespace(tag_v1_resource, path=f'/v1/{tag_path}')

    return app