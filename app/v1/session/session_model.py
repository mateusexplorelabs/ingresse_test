from db import Base
from sqlalchemy import Column, Integer, DateTime, ForeignKey
from marshmallow_sqlalchemy import ModelSchema

class Session(Base):
    __tablename__ = 'sessions'
    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, ForeignKey('events.id'))
    date_time = Column(DateTime)  

class SessionSchema(ModelSchema):
    class Meta:
        model = Session