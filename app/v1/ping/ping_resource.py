from flask_restplus import Resource, Namespace

route_name = 'ping'
ns = Namespace(name=route_name)

@ns.route('')
@ns.doc(f'health check')
class PingResource(Resource):

    @ns.doc(f'ping ...')
    def get(self):
        return {
            "message": "Pong!"
        }