import logging
from sqlalchemy import asc, desc
from .errors import CustomError
from db import Session

logger = logging.getLogger(__name__)

class Service(object):

    __model__ = None
    __schema__ = None
   
    def page(self, args, query=None):
        logger.info(f'Page from {self.__model__}')
        limit = 10 if not args.get('limit') else args.get('limit')
        offset = 0 if not args.get('offset') else args.get('offset')
        if not query:
            query = Session.query(self.__model__).limit(limit).offset(offset)
        else:
            query = query.limit(limit).offset(offset)
        model_list = query.all()
        return self._make_list_response(model_list)

    def get(self, id, only_search=False):
        logger.info(f'Get by id from {self.__model__}')
        model = Session.query(self.__model__).get(id)
        if not model:
            raise CustomError(status=404, code=1001, message="Resource not found")
        if only_search:
            return model
        return self._make_model_response(model)

    def create(self, payload):   
        logger.info(f'Create {self.__model__}')
        model = self._json_to_model(payload)    
        Session.add(model)
        Session.commit()
        return self._make_model_response(model)

    def update(self, id, payload): 
        logger.info(f'Update {self.__model__}')
        model = self._json_to_model(payload)
        if model.id != id:
            raise CustomError(status=422, code=1004, message=f'Both ids must be the same. id param: {id} != id request: {model.id}')
        Session.merge(model)
        Session.commit()
        return self._make_model_response(model)

    def delete(self, id):
        logger.info(f'Delete {self.__model__}')
        model = self.get(id=id, only_search=True)
        Session.delete(model)
        Session.commit()
        return self._make_model_response(model)
        
    def _json_to_model(self, payload):
        if not payload:
            raise CustomError(status=400, code=1002, message='Empty request')
        try:
            data = self.__schema__.load(payload, session=Session)
        except Exception as ex:
            logger.error(ex)
            raise CustomError(status=422, code=1003, message=f'Request has no valid schema: {self.__model__}')
        return data

    def _make_model_response(self, model, is_list=False):
        schema = self.__schema__.dump(model, many=is_list)
        response = {
            "data": schema
        }
        return response

    def _make_list_response(self, model_list):
        return self._make_model_response(model=model_list, is_list=True)
