import os
import logging

from flask import jsonify

logger = logging.getLogger(__name__)

class CustomError(Exception):
        
    def __init__(self, status=500, code=500, message=None):

        super().__init__(self)
        self.status = status
        self.code = code
        self.message = message
       
    def to_json(self):

        return {
            'status': self.status,
            'code': self.code,
            'message': self.message
        }

def handle_custom_error(error):
    logger.error(error.to_json())
    return jsonify(error.to_json()), error.status

def register_custom_error_handler(app):
    app.register_error_handler(CustomError, handle_custom_error)
