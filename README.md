# Events Management API #

## About ##

Events Management API is destinated for a interview test on Ingresse, is a little solution for events search, save, update and delete. This API also manage a interests views on witch event, who everyone can be interested.

## Requirements ##

- [python 3.6 or higher](https://www.python.org/downloads/)
- virtualenv 
- [docker-compose](https://docs.docker.com/install/)

## Settings ##

After python and virtualenv instaled we need create a virtual place for work, they name will venv, then activate venv, for both execute those follow commands:

```
virtualenv venv
source venv/bin/activate
```

To work on this project is necessary install all requirements, we can do this with this command:

```
pip install -r requirements.txt
```

If you preffer, it's possible execute those settings above with a make command:
```
make install
```
This script will install python, virtualenv, create a virtual place for work and install all requirements for application

## Database Settings ##

Initialize a database with a docker-compose command:
```
docker-compose up -d
```
Check if port 3306 is free for use!

Create a database tables with migrations python script:
```
python migrate.py
```

## Tests ##

For tests is use pytest, so can be execute with this code:

```
pytest
```

pytest give us a check coverage for aplication project:

```
pytest --cov ./app
```
If you have a difficulty with a pytest coverage [this link](https://pypi.org/project/pytest-cov/) can be helpful.

If you preffer its possible execute all tests commands with make scripts.
Tests:

```
make test
```
Coverage:

```
make coverage
```
Coverage command will execute tests, coverage and make a xml coverage for a code quality preview.


## Run ##

For run application just execute a run.py script into a root folder:
```
python run.py
```
The application starts on port 5000, go on localhost:5000 you will se a full routes swagger documentation for event API.

## Code Quality ##

For preserve a quality code, this project use a [sonarqube](https://www.sonarqube.org/) and sonnar-scanner tools. 
This tool presents a report with coverage, quality, duplications and vulnerabilities. 
To take this report previously initialize a docker container with a sonarqube:
```
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```
If you already execute this command to create a docker container, just run:
```
docker start sonarqube
```
To take a full coverage of state of project, before sonnar-scanner execution make a pytest coverage, and then make a xml coverage with this command:
```
coverage xml -i
```
Then execute a sonar-scanner docker container, on project root folder:
```
docker run -ti -v $(pwd):/usr/src --link sonarqube newtmitch/sonar-scanner
```
Make sure that sonarqube container is running.
sonar-scanner should enable a web page on port 9000 with project results, go on localhost for see this.

If you have a difficulty with a sonarqube ou sonar-scanner [this link](https://github.com/newtmitch/docker-sonar-scanner) can be helpful.

## Commits on repository ##

This projects follow a specification [Conventional Commits](https://www.conventionalcommits.org/) for commits descriptions