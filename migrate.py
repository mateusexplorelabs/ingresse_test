from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from run import create_app
from db import init_db


if __name__ == '__main__':
    app = create_app()
    init_db()