import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
from sqlalchemy.ext.declarative import declarative_base

load_dotenv()   

Base = declarative_base()

engine = create_engine(os.environ['SQLALCHEMY_DATABASE_URI'])

Session = scoped_session(sessionmaker(bind=engine))

def init_db():
    Base.metadata.drop_all(bind=engine)
    from app import models
    Base.metadata.create_all(bind=engine)